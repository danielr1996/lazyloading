package de.arbeitsagentur.lazyloading.eagerloadingWithDomainService;

public class DomainService {
    public boolean isAllowed(boolean firstValue, boolean secondValue, boolean thirdValue){
        return firstValue && secondValue && thirdValue;
    }
}
