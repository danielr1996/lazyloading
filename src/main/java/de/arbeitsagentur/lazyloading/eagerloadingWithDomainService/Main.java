package de.arbeitsagentur.lazyloading.eagerloadingWithDomainService;

import de.arbeitsagentur.lazyloading.Repository;

public class Main {
    private static Repository repository = new Repository();
    private static DomainService domainService = new DomainService();

    public static void main(String[] args) {
        System.out.println("isAllowed: "+domainService.isAllowed(repository.getFirstValue(), repository.getSecondValue(), repository.getThirdValue()));
    }
}
