package de.arbeitsagentur.lazyloading.eagerloading;

import de.arbeitsagentur.lazyloading.Repository;

public class Main {
    static Repository repository = new Repository();

    private static boolean isAllowed() {
        return repository.getFirstValue() && repository.getSecondValue() && repository.getThirdValue();
    }

    public static void main(String[] args) {
        System.out.println("isAllowed: "+isAllowed());
    }
}
