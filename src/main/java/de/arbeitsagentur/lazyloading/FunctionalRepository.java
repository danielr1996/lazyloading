package de.arbeitsagentur.lazyloading;

import java.util.function.BooleanSupplier;

public class FunctionalRepository {
    public BooleanSupplier getFirstValue(){
        return ()->{
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("getFirstValue: true");
            return true;
        };
    }

    public BooleanSupplier getSecondValue(){
        return ()->{
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("getSecondValue: false");
            return false;
        };
    }

    public BooleanSupplier getThirdValue(){
        return ()->{
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("getThirdValue: false");
            return false;
        };
    }
}
