package de.arbeitsagentur.lazyloading;

public class Repository {
    public boolean getFirstValue(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("getFirstValue: true");
        return true;
    }

    public boolean getSecondValue(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("getSecondValue: false");
        return false;
    }

    public boolean getThirdValue(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("getThirdValue: false");
        return false;
    }
}
