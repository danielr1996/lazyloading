package de.arbeitsagentur.lazyloading.lazyloadingWithFunctionalRepository;

import java.util.function.BooleanSupplier;

public class DomainService {
    public boolean isAllowed(BooleanSupplier firstValue, BooleanSupplier secondValue, BooleanSupplier thirdValue){
        return firstValue.getAsBoolean() && secondValue.getAsBoolean() && thirdValue.getAsBoolean();
    }
}
