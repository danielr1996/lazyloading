package de.arbeitsagentur.lazyloading.lazyloadingWithFunctionalRepository;

import de.arbeitsagentur.lazyloading.FunctionalRepository;

public class Main {
    private static FunctionalRepository repository = new FunctionalRepository();
    private static DomainService domainService = new DomainService();

    public static void main(String[] args) {
        System.out.println("isAllowed: "+domainService.isAllowed(repository.getFirstValue(), repository.getSecondValue(), repository.getThirdValue()));
    }
}
