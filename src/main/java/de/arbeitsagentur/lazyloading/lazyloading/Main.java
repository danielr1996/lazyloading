package de.arbeitsagentur.lazyloading.lazyloading;

import de.arbeitsagentur.lazyloading.Repository;

import java.util.function.BooleanSupplier;

public class Main {
    private static Repository repository = new Repository();
    private static DomainService domainService = new DomainService();

    public static void main(String[] args) {
        BooleanSupplier firstValue = ()->repository.getFirstValue();
        BooleanSupplier secondValue = ()->repository.getSecondValue();
        BooleanSupplier thirdValue = ()->repository.getThirdValue();

        System.out.println("isAllowed: "+domainService.isAllowed(firstValue, secondValue, thirdValue));
    }
}
